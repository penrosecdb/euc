<?php
function bootstrap_script_enqueue(){	
wp_enqueue_style ('customstyle', get_template_directory_uri() . '/css/style.css', array(), NULL, NULL);	
wp_enqueue_style ('customstyle', get_template_directory_uri() . '/css/bootstrap.min.css', array(),NULL, NULL);
wp_enqueue_style ('customstyle', get_template_directory_uri() .'/css/swiper.min.css', array(),NULL, NULL);
wp_enqueue_script ('customjs', get_template_directory_uri() .'/js/bootstrap.min.js', array('jquery'),false,true);
wp_enqueue_script ('customjs', get_template_directory_uri() .'/js/swiper.min.js', array('jquery'),false,true);	
wp_enqueue_script ('customjs', get_template_directory_uri() .'/js/jquery.min.js', array('jquery'),false,true);	
}
add_action('wp_enqueue_scripts', 'bootstrap_script_enqueue');

function register_theme_menus(){
	
	register_nav_menu('Main_Menu' ,'MainMenu');
}
add_action('init', 'register_theme_menus');
?>
<?php
function myphpinformation_scripts() {    


    if( !is_admin()){
 wp_deregister_script('jquery');
 wp_deregister_script('jquery', get_stylesheet_directory_uri() . '/js/jquery.min.js', false);
 wp_enqueue_script('jquery');
}

}

add_action( 'wp_enqueue_scripts', 'myphpinformation_scripts' );
?>
<?php
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

add_theme_support("post-thumbnails");

add_image_size('cover',400,566,array( 'center', 'center' ));
add_image_size('cover-small',300,425,array( 'center', 'center' ));

add_filter( 'rwmb_meta_boxes', 'euc_register_meta_boxes' );
 
function euc_register_meta_boxes( $meta_boxes ) {
    $prefix = 'rw_';
    // 1st meta box
    $meta_boxes[] = array(
        'id'         => 'pubcardc',
        'title'      => __( 'Report Details' ),
        'post_types' => array( 'post'),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Report Files'),
                'desc'  => 'Upload related files here',
                'id'    => $prefix . 'files',
                'type'  => 'file_advanced',
                'std'   => '',
                'class' => '',
            ),
            array(
                'name'  => __( 'Author Name'),
                'desc'  => "Name of the report's author",
                'id'    => $prefix . 'author',
                'type'  => 'text',
                'std'   => '',
                'class' => '',
            ),
        )
    );
    return $meta_boxes;
}
?>