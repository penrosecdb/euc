<?php /* Template Name: aboutus*/

get_header();


?>


<body id="aboutus" <? php body_class(); ?>>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand navbar-link" href="/"><img class="img-circle" src="<?php bloginfo('template_directory');?>/img/EUC_Symbol.png" alt="90" width="50" height="50" id="logo"> </a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
				
				<?php

				$args= array(
					'theme_location'=> 'Main_Menu',
					'menu'          => 'MainMenu',
					'menu_class'	=> 'nav navbar-nav navbar-right',
					'menu_id'		=>'menu-mainmenu',
					'container'		=> 'div',
					'container_class' => 'container-fluid',
					
				);
				wp_nav_menu($args);

				?>
				
               
            </div>
        </div>
    </nav>


<img id="map" src="<?php bloginfo('template_directory');?>/img/mapv2.png">
    <div id="Description">
        <div class="jumbotron"><img src="<?php bloginfo('template_directory');?>/img/Logo.png" width="320" id="biglogo">
            <h2 id="bigheading1"><strong>About "Energy Union Choices"</strong> </h2>
            <p></p>
        </div>
    </div>




<?php
$args = array(
						'posts_per_page'   => 10,
						'category_name'	   => 'aboutus',
						'category'    => 'aboutus',
						'offset'           => 0,
						'include'          => '',
						'exclude'          => '',
						'meta_key'         => '',
						'meta_value'       => '',
						'post_type'        => 'post',
						'post_mime_type'   => '',
						'post_parent'      => '',
						'author'	   => '',
						'author_name'	   => '',
						'post_status'      => 'publish',
						'suppress_filters' => true
					);
					$posts=get_posts($args);
				foreach ($posts as $post){?>

<div id="section1">

        <div id="who-card">
            <div id="about-img" style="background-image:url(<?php $image=wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
			echo $image[0] ?>);" ></div>
            <div id="who-content"><h2 id="title1_aboutus"><?php echo $post->post_title; ?></h2>
            <p id="about_ustext"><?php echo apply_filters("the_content",$post->post_content); ?> </p>

            </div>
        </div>


   </div>
 

<?php }
				?>

 <?php get_footer(); ?>
    