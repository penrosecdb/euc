﻿	 
            <div id="End">
                <div class="col-md-4" id="endheading">
                    <h3 id="contactustext">Feel Free To contact us </h3></div>
                <div class="col-md-4" id="endp1">
                    <p><strong>Press Contact</strong></br> <strong>Myriam Castanie:</strong> myriam.castanie@europeanclimate.org </p>
                </div>
                <div class="col-md-4" id="endp2">
                    <p><strong>General Enquiries</strong></br> <strong>Stijn Carton:</strong> stijn.carton@europeanclimate.org&nbsp; </p>
                </div>
		<div class="" id="endp3">
  
                </div>
		
            </div>
        
 <footer id="footerpage">
    <div id="footer">
        
            <h4 id="copyright"><strong>© Copyright Energy Choices Union 2016.</br> All Rights Reserved.</strong> </h4>
            <h4 id="termsofuse" link=""><strong><a href="#" class="show-terms">Terms Of Use</a></strong></h4> </div> </footer>



            <div id="popup_showterms">
        
        <div id="cond-container">
            <a href="#" id="close-terms">X</a>
            <p>
                PRIVACY<br>
The Energy Union Choice Initiative respects your privacy and takes necessary steps to protect the information collected through <a href="http://www.energyunionchoices.eu/staging/www.energyunionchoices.eu">www.energyunionchoices.eu</a>.
 Like many other sites, we track visitors to our website using Google 
Analytics. Data collected can include information about your computer, 
your IP address, geographical location, browser type, referral source, 
length of visit, number of page views, and which pages you visited. The 
reports we get from Google Analytics help us understand how people are 
using our site and make it more useful.
            </p>
            <p>
                CONCERNS?<br>
We welcome your questions and comments about online privacy. Please feel
 free to contact us with any privacy concerns at 
info@europeeanclimate.org.
            </p>
        </div></div>
      
   



    </div>
   <script src="<?php bloginfo('template_directory');?>/js/jquery.js"></script>
   

	
	<script src="<?php bloginfo('template_directory');?>/js/swiper.min.js"></script>
	 <script src="<?php bloginfo('template_directory');?>/js/css_clider.js"></script>	
		
	
 	

		
<script>
   var swiper = new Swiper('.swiper-container', {
      
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        effect: "coverflow",
        slidesPerView: 2.5,
	slideToClickedSlide: true,
       centeredSlides: true,
     	 
   	

        coverflow: {
            rotate: 0,
            stretch:0,
            depth: 60,
            modifier: 16,
           slideShadows : false,
         
        }
    
       
    });

 </script>




		
		
     <script>
    

 $(".show-terms").click(function(e){
        e.preventDefault();
        $("#popup_showterms").fadeIn();
    });

 $("#close-terms").click(function(e){
        e.preventDefault();
        $("#popup_showterms").fadeOut();
    });


</script>

	<?php wp_footer(); ?>	
		
</body>
</html>
	