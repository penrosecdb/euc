﻿<?php get_header(); ?>


<body id="home" <? php body_class(); ?>>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header page-scroll">
                <a class="navbar-brand navbar-link" href="/"><img class="img-circle" src="<?php bloginfo('template_directory');?>/img/EUC_Symbol.png" alt="90" width="50" height="50" id="logo"> </a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
				
				<?php

				$args= array(
					'theme_location'=> 'Main_Menu',
					'menu'          => 'MainMenu',
					'menu_class'	=> 'nav navbar-nav navbar-right',
					'menu_id'		=>'menu-mainmenu',
					'container'		=> 'div',
					'container_class' => 'container-fluid',
					
				);
				wp_nav_menu($args);

				?>
				
               
            </div>
        </div>
    </nav>


<div id="Section1img">
<img id="map" src="<?php bloginfo('template_directory');?>/img/mapv2.png">
    <div id="Description">
        <div class="jumbotron"><img src="<?php bloginfo('template_directory');?>/img/Logo.png" width="320" id="biglogo">
            <h4 class="text-left text-primary"><strong>Introducing “Energy Union Choices”</strong> </h4>
            <p class="text-left">The mission of the Energy Union Choices is to provide practical, independent and objective analysis on the choices and decisions required to remain on track to a low carbon economy. It is an innovative way of engaging and testing critical
                choices around infrastructure deployment, energy security trade-offs, system flexibility and value of integration.</p>
            <a class="btn btn-default btn-lg" id="aboutusindexbtn" role="button" href=" http://www.energyunionchoices.eu/who-we-are/"><p id="pubbtn">Find Out More About us</p></a>
       		<hr id="separatorj"></hr>	
		
 </div>
        <p class="text-justify text-info" id="reporttitle"><strong>Our Reports</strong></p>
    </div>


    
      </div>


<div id="Section2img">

<?php get_template_part( 'css_slider' ); ?>

 </div> 
  
 

<div id="Section3img">
    <div id="publications">
        <a role="link" href="http://www.energyunionchoices.eu/reports/" class="btn btn-default btn-lg"  id="publicationslink" > <p id="pubbtn">View All Publications</p> </a>
    </div>
  


 </div> 

    <?php get_footer(); ?>

