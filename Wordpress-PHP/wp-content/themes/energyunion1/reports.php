<?php /* Template Name: pubreports */
	
?>

<!DOCTYPE html>
<html lang="en-us" style="margin-top:0px !important;">

<head>

	<?php get_header(); ?>
	<title>EUC</title>

	
</head>






<body id="reportspub" <? php body_class(); ?>>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand navbar-link" href="/"><img class="img-circle" src="<?php bloginfo('template_directory');?>/img/EUC_Symbol.png" alt="90" width="50" height="50" id="logo"> </a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
				
				<?php

				$args= array(
					'theme_location'=> 'Main_Menu',
					'menu'          => 'MainMenu',
					'menu_class'	=> 'nav navbar-nav navbar-right',
					'menu_id'		=>'menu-mainmenu',
					'container'		=> 'div',
					'container_class' => 'container-fluid',
					
				);
				wp_nav_menu($args);

				?>
				
               
            </div>
        </div>
    </nav>


<div id="Section1img">

<img id="map" src="<?php bloginfo('template_directory');?>/img/mapv2.png">

    <div id="Description">
        <div class="jumbotron"><img src="<?php bloginfo('template_directory');?>/img/Logo.png" width="320" id="biglogo">
            <p></p>
        </div>
    </div>
    <p class="text-justify text-info" id="reporttitle1"><strong>Our Reports</strong></p>

<div>

	<?php get_template_part( 'css_slider1' ); ?>

</div>

			


	
<?php
					$args = array(
						'posts_per_page'   => 10,
						'offset'           => 0,
                        			'category_name'	   => 'publication',
						'category'   	  => 'publication',
						
						'include'          => '',
						'exclude'          => '',
						'meta_key'         => '',
						'meta_value'       => '',
						'post_type'        => 'post',
						'post_mime_type'   => '',
						'post_parent'      => '',
						'author'	   => '',
						'author_name'	   => '',
						'post_status'      => 'publish',
						'suppress_filters' => true
					);
					$posts=get_posts($args);
				foreach ($posts as $post){?>

	<section id="sectionpublications">
	



	

	<div class="pubcardc2" id="<?php echo $post->post_name;?>"  >

		<div class="col-md-5" id="image">
			<?php $image=wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'cover-small' );
			echo "<img class='' src='".$image[0]."' />"; ?>
		</div>

		
 			<div id="contentpub1">
			
			 <h3 id="h3_1"> <?php echo $post->post_title; ?> </h3>
			 <hr id="ligne1">

			<p class="text-justify" id="pub1parag"> <?php echo apply_filters("the_content",$post->post_content); ?> </p>
			<p id="p2pub1"><strong> BY: <?php echo rwmb_meta("rw_author",'',$post->ID); ?> - DATE: <?php echo get_the_date("d.m.Y",$post->ID); ?>   </strong> </p>
			
        		<div class="dropdown" id="list">
                    
				<button class="btn btn-default btn-lg dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button" id="buttondropdown"><i class="fa fa-download" aria-hidden="true"></i></button>
                    
				<ul class="dropdown-menu" role="menu" id="dropdown_menu_content">      
				<?php $files = rwmb_meta( 'rw_files', '', $post->ID );
							    if ( !empty( $files ) ) {
							        foreach ( $files as $file ) {
							            echo "<li><a class='dropdownmenuitem' href='{$file['url']}' title='{$file['title']}'>{$file['title']}</a></li>";
							        }
							    } ?> 	
         		</ul> </div>

	</div></div>
     </section>
				
	<?php }
				?>
	
	<?php get_footer(); ?>
