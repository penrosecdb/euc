<div id="reports">
<div class="swiper-container">


 	<div class="swiper-wrapper">
	  
		
	


            <?php				
					
			
						
						$args = array(
							'posts_per_page'   => -1,
							'offset'           => 0,
							'category_name'	   => 'publication',
							'category'   	  => 'publication',
							'include'          => '',
							'order'            => 'ASC',
							'exclude'          => '',
							'meta_key'         => '',
							'meta_value'       => '',
							'post_type'        => 'post',
							'post_mime_type'   => '',
							'post_parent'      => '',
							'author'	   => '',
							'author_name'	   => '',
							'post_status'      => 'publish',
							'suppress_filters' => true,
						);
						$posts=get_posts($args);

			
			foreach ($posts as $post){
			echo    '<div class="swiper-slide">';									
			$image=wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'cover'); 
			echo "<img src='".$image[0]."' href='#".$post->post_name."'>";
			echo  '</div>';
						}
					
					

  


			?>  
  </div>
     
     
	<!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>  
       
    	  
</div>	
 
</div>





<?php
					$args = array(
						'posts_per_page'   => -1,
							'offset'           => 0,
							'category_name'	   => 'publication',
							'category'   	  => 'publication',
							'order'            => 'ASC',
							'include'          => '',
							'exclude'          => '',
							'meta_key'         => '',
							'meta_value'       => '',
							'post_type'        => 'post',
							'post_mime_type'   => '',
							'post_parent'      => '',
							'author'	   => '',
							'author_name'	   => '',
							'post_status'      => 'publish',
							'suppress_filters' => true,
					);
					$posts=get_posts($args);
				foreach ($posts as $post){?>



					
	<section id="sectionpublications">
	



	

	<div class="pubcardc" id="<?php echo $post->post_name;?>"  >

		<div class="col-md-5" id="image">
			<?php $image=wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'cover-small' );
			echo "<img class='' src='".$image[0]."' />"; ?>
		</div>

		
 			<div id="contentpub1">
			
			 <h3 id="h3_1"> <?php echo $post->post_title; ?> </h3>
			 <hr id="ligne1">

			<p class="text-justify" id="pub1parag"> <?php echo apply_filters("the_content",$post->post_content); ?> </p>
			<p id="p2pub1"><strong> BY: <?php echo rwmb_meta("rw_author",'',$post->ID); ?> - DATE: <?php echo get_the_date("d.m.Y",$post->ID); ?>   </strong> </p>
			
        		<div class="dropdown" id="list">
                    
				<button class="btn btn-default btn-lg dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button" id="buttondropdown"><i class="fa fa-download" aria-hidden="true"></i></button>
              <ul class="dropdown-menu" role="menu" id="dropdown_menu_content">      
				<?php $files = rwmb_meta( 'rw_files', '', $post->ID );
							    if ( !empty( $files ) ) {
							        foreach ( $files as $file ) {
							            echo "<li><a class='dropdownmenuitem' href='{$file['url']}' title='{$file['title']}'>{$file['title']}</a></li>";
							        }
							    } ?> 	
         		</ul> </div>

	</div>
     </div>
			</section>	
	<?php }
				?>
	



